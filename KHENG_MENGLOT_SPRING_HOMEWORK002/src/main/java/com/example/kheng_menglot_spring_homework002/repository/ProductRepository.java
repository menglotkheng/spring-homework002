package com.example.kheng_menglot_spring_homework002.repository;

import com.example.kheng_menglot_spring_homework002.dto.CustomerDTO;
import com.example.kheng_menglot_spring_homework002.dto.ProductDTO;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {


    //get all
    @Select("SELECT * FROM product_tb")
    @Results(
            id = "id", value = {
            @Result(property = "id", column = "product_id"),
            @Result(property = "name", column = "product_name"),
            @Result(property = "price", column = "product_price"),
    }

    )
    List<ProductDTO> getAll();


    //get by id
    @Select("SELECT * FROM product_tb WHERE product_id= #{id}")
    @ResultMap("id")
    ProductDTO getById(Integer id);

    //insert
    @Select("INSERT into product_tb(product_name, product_price)\n" +
            "VALUES (#{pd.name},#{pd.price}) returning *")
    @ResultMap("id")
    ProductDTO insert(@Param("pd") ProductDTO product);

    //delete
    @Delete("DELETE FROM product_tb WHERE product_id=#{id}")
    Integer delete(Integer id);

    //update
    @Select("UPDATE product_tb\n" +
            "SET product_name = #{pd.name}, product_price = #{pd.price} \n" +
            "WHERE product_id=#{id} returning *;")
    Integer update(Integer id, @Param("pd") ProductDTO product);

}
