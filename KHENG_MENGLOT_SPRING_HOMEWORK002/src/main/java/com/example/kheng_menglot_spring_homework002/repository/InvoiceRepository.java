package com.example.kheng_menglot_spring_homework002.repository;

import com.example.kheng_menglot_spring_homework002.dto.InvoiceDTO;
import com.example.kheng_menglot_spring_homework002.entity.Invoice;
import com.example.kheng_menglot_spring_homework002.entity.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {

    //get all product
    @Select("SELECT * FROM invoice_detail_tb INNER JOIN product_tb pt on pt.product_id = invoice_detail_tb.product_id WHERE invoice_id=#{id}")
    @Result(property = "id", column = "product_id")
    @Result(property = "name", column = "product_name")
    @Result(property = "price", column = "product_price")
    List<Product> getAllProduct(Integer id);


    //get all invoice
    @Select("select * from invoice_tb")
    @Results(
            id = "invoice",
            value = {
                    @Result(property = "id", column = "invoice_id"),
                    @Result(property = "date", column = "invoice_date"),
                    @Result(property = "customerId", column = "customer_id", one = @One(select = "com.example.kheng_menglot_spring_homework002.repository.CustomerRepository.getById")),
                    @Result(property = "products", column = "invoice_id", many = @Many(select = "getAllProduct")),
            }
    )
    List<Invoice> getAll();


    //insert invoice
    @Select("insert into invoice_tb(invoice_date,customer_id) values(#{invoice.date},#{invoice.customerId})" +
            "returning invoice_id")
    Integer insertInvoice(@Param("invoice") InvoiceDTO invoice);

    //get by id
    @Select("select * from invoice_tb where invoice_id=#{id}")
    @ResultMap("invoice")
    Invoice getInvoiceById(Integer id);

    @Insert("insert into invoice_detail_tb(invoice_id,product_id) values(#{invoiceId},#{productId})")
    void insertInvoiceDetail(Integer invoiceId, Integer productId);

    //update by id
    @Select("update invoice_tb set invoice_date=#{invoices.date},customer_id=#{invoice.customerId} where invoice_id=#{id}" +
            "returning invoice_id")
    Integer updateInvoiceById(Integer id, @Param("invoices") InvoiceDTO invoices);

    //delete by id
    @Delete("delete from invoice_detail_tb where invoice_id=#{id}")
    Integer deleteInvoice(Integer id);

}
