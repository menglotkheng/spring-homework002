package com.example.kheng_menglot_spring_homework002.repository;

import com.example.kheng_menglot_spring_homework002.dto.CustomerDTO;
import lombok.AllArgsConstructor;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {


    //get all
    @Select("SELECT * FROM customer_tb")
    @Results(
            id = "id", value = {
            @Result(property = "id", column = "customer_id"),
            @Result(property = "name", column = "customer_name"),
            @Result(property = "address", column = "customer_address"),
            @Result(property = "phone", column = "customer_phone")
    }

    )
    List<CustomerDTO> getAll();


    //get by id
    @Select("SELECT * FROM customer_tb WHERE customer_id= #{id}")
    @ResultMap("id")
    CustomerDTO getById(Integer id);

    //insert
    @Select("INSERT into customer_tb(customer_name, customer_address, customer_phone)\n" +
            "VALUES (#{cs.name},#{cs.address},#{cs.phone} ) returning *")
    @ResultMap("id")
    CustomerDTO insert(@Param("cs") CustomerDTO customer);

    //delete
    @Delete("DELETE FROM customer_tb WHERE customer_id=#{id}")
    Integer delete(Integer id);

    //update
    @Select("UPDATE customer_tb\n" +
            "SET customer_name = #{cs.name}, customer_address = #{cs.address}, customer_phone =#{cs.phone}\n" +
            "WHERE customer_id=#{id} returning *;")
    Integer update(Integer id, @Param("cs") CustomerDTO customer);

}
