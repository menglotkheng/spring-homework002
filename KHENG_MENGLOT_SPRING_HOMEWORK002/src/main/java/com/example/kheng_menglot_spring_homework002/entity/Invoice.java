package com.example.kheng_menglot_spring_homework002.entity;

import com.example.kheng_menglot_spring_homework002.dto.CustomerDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {

    private Integer id;
    private Timestamp date;
    private CustomerDTO customerId;

    private List<Product> products;


}
