package com.example.kheng_menglot_spring_homework002.dto;

import com.example.kheng_menglot_spring_homework002.entity.Customer;
import com.example.kheng_menglot_spring_homework002.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceDTO {

    private Integer id;
    private LocalDateTime date;
    private Integer customerId;

    private List<Integer> products;


}
