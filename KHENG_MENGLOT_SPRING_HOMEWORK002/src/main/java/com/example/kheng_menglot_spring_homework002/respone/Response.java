package com.example.kheng_menglot_spring_homework002.respone;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response<T> {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private String message;
    private Boolean success;

}
