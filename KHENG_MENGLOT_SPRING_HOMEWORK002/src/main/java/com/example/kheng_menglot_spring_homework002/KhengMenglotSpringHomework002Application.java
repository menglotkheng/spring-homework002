package com.example.kheng_menglot_spring_homework002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KhengMenglotSpringHomework002Application {

    public static void main(String[] args) {
        SpringApplication.run(KhengMenglotSpringHomework002Application.class, args);
    }

}
