package com.example.kheng_menglot_spring_homework002.controller;

import com.example.kheng_menglot_spring_homework002.dto.CustomerDTO;
import com.example.kheng_menglot_spring_homework002.dto.ProductDTO;
import com.example.kheng_menglot_spring_homework002.respone.Response;
import com.example.kheng_menglot_spring_homework002.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/product")
public class ProductController {

    @Autowired
    private ProductServices services;

    //get all customer
    @GetMapping(path = "/get")
    public ResponseEntity<?> get() {
        return ResponseEntity.ok(new Response<List<ProductDTO>>(services.getAll(), "Sucess", true));
    }

    //get by id
    @GetMapping(path = "/get/{id}")
    public ResponseEntity<?> getById(@PathVariable(value = "id") Integer id) {

        return ResponseEntity.ok(
                new Response<ProductDTO>(services.getById(id), "Success", true)
        );
    }

    //insert
    @PostMapping(path = "/insert")
    public ResponseEntity<?> insert(@RequestBody ProductDTO product) {

        return ResponseEntity.ok(
                new Response<ProductDTO>(services.insert(product), "Success", true)
        );
    }

    //delete
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {


        if (services.delete(id) == 1) {
            return ResponseEntity.ok(
                    new Response<ProductDTO>(null, "Delete Success", true)
            );
        }
        return ResponseEntity.ok().body("Not Found !!!");

    }

    //update
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@PathVariable(value = "id") Integer id, @RequestBody ProductDTO product) {

        if (services.update(id, product) == 1) {
            return ResponseEntity.ok(
                    new Response<ProductDTO>(null, "Product Update Success", true)
            );
        }
        return ResponseEntity.ok().body("Not Found !!!");
    }

}
