package com.example.kheng_menglot_spring_homework002.controller;

import com.example.kheng_menglot_spring_homework002.dto.InvoiceDTO;
import com.example.kheng_menglot_spring_homework002.entity.Invoice;
import com.example.kheng_menglot_spring_homework002.respone.Response;
import com.example.kheng_menglot_spring_homework002.services.InvoiceServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/invoice/api/v1")
public class InvoiceController {

    @Autowired
    private InvoiceServices services;

    //get all
    @GetMapping(path = "/get")
    public ResponseEntity<?> get() {
        return ResponseEntity.ok(
                new Response<>(services.getAll(), "successful", true)
        );
    }

    //get by id
    @GetMapping("/get/{id}")
    public ResponseEntity<?> getAllInvoice(@PathVariable("id") Integer id) {

        if (services.getById(id) != null) {
            return ResponseEntity.ok().body(
                    new Response<>(services.getById(id), "Successful", true)
            );
        }
        return ResponseEntity.notFound().build();
    }

    //insert
    @PostMapping("/insert")
    public ResponseEntity<?> insert(@RequestBody InvoiceDTO invoice) {
        Integer invoice_id = services.insertInvoice(invoice);
        Invoice invoices = services.getById(invoice_id);

        return ResponseEntity.ok(new Response<Invoice>(invoices, "insert successful", true));
    }

    @PutMapping("update/{id}")
    public ResponseEntity<?> updateInvoice(@PathVariable("id") Integer id, InvoiceDTO invoice) {
        Integer invoice_id = services.updateInvoice(id, invoice);
        Invoice invoices = services.getById(invoice_id);

        if (invoice != null) {
            return ResponseEntity.ok().body(
                    new Response<>(invoices, "successful", true)
            );
        }
        return ResponseEntity.notFound().build();
    }
    @DeleteMapping("delete/invoice/{id}")
    public ResponseEntity<?> deleteInvoice(@PathVariable("id") Integer id) {
        Integer deleted=  services.deleteInvoice(id);
        if (deleted==1) {
            return ResponseEntity.ok().body(
                    new Response<>(null, "deleted invoice successful", true)
            );
        }
        return ResponseEntity.notFound().build();
    }

}



