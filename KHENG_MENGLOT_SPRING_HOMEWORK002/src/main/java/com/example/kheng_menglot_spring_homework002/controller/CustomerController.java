package com.example.kheng_menglot_spring_homework002.controller;

import com.example.kheng_menglot_spring_homework002.dto.CustomerDTO;
import com.example.kheng_menglot_spring_homework002.respone.Response;
import com.example.kheng_menglot_spring_homework002.services.CustomerServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/customer")
public class CustomerController {

    @Autowired
    private CustomerServices services;

    //get all customer
    @GetMapping(path = "/get")
    public ResponseEntity<?> get() {
        return ResponseEntity.ok(new Response<List<CustomerDTO>>(services.getAll(), "Sucess", true));
    }

    //get by id
    @GetMapping(path = "/get/{id}")
    public ResponseEntity<?> getById(@PathVariable(value = "id") Integer id) {

        return ResponseEntity.ok(
                new Response<CustomerDTO>(services.getById(id), "Success", true)
        );
    }

    //insert
    @PostMapping(path = "/insert")
    public ResponseEntity<?> insert(@RequestBody CustomerDTO customer) {

        return ResponseEntity.ok(
                new Response<CustomerDTO>(services.insert(customer), "Success", true)
        );
    }

    //delete
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Integer id) {


        if (services.delete(id) == 1) {
            return ResponseEntity.ok(
                    new Response<CustomerDTO>(null, "Delete Success", true)
            );
        }
        return ResponseEntity.ok().body("Not Found !!!");

    }

    //update
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@PathVariable(value = "id") Integer id, @RequestBody CustomerDTO customer) {

        if (services.update(id, customer) == 1) {
            return ResponseEntity.ok(
                    new Response<CustomerDTO>(null, "Customer Update Success", true)
            );
        }
        return ResponseEntity.ok().body("Not Found !!!");
    }

}
