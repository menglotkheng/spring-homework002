package com.example.kheng_menglot_spring_homework002.services;

import com.example.kheng_menglot_spring_homework002.dto.CustomerDTO;


import java.util.List;

public interface CustomerServices {

    //insert
    CustomerDTO insert(CustomerDTO customer);

    //get by id
    CustomerDTO getById(Integer id);

    //get all
    List<CustomerDTO> getAll();

    //delete
    Integer delete(Integer id);

    //update
    Integer update(Integer id, CustomerDTO customer);


}
