package com.example.kheng_menglot_spring_homework002.services;


import com.example.kheng_menglot_spring_homework002.dto.ProductDTO;

import java.util.List;

public interface ProductServices {

    //insert
    ProductDTO insert(ProductDTO product);

    //get by id
    ProductDTO getById(Integer id);

    //get all
    List<ProductDTO> getAll();

    //delete
    Integer delete(Integer id);

    //update
    Integer update(Integer id, ProductDTO product);

}
