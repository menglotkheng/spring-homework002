package com.example.kheng_menglot_spring_homework002.services.impl;

import com.example.kheng_menglot_spring_homework002.dto.InvoiceDTO;
import com.example.kheng_menglot_spring_homework002.entity.Invoice;
import com.example.kheng_menglot_spring_homework002.repository.InvoiceRepository;
import com.example.kheng_menglot_spring_homework002.services.InvoiceServices;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
@AllArgsConstructor
public class InvoiceServicesImpl implements InvoiceServices {

    private InvoiceRepository repository;


    //get by id
    @Override
    public Invoice getById(Integer id) {
        return repository.getInvoiceById(id);
    }

    //get all
    @Override
    public List<Invoice> getAll() {
        return repository.getAll();
    }

    //insert
    @Override
    public Integer insertInvoice(InvoiceDTO invoice) {
        Integer id = repository.insertInvoice(invoice);
        for (int i = 0; i < invoice.getProducts().size(); i++) {
            repository.insertInvoiceDetail(id, invoice.getProducts().get(i));
        }

        return id;
    }
    @Override
    public Integer updateInvoice(Integer id, InvoiceDTO invoice) {
        Integer in_id= repository.updateInvoiceById(id, invoice);
        repository.deleteInvoice(in_id);
        for (int i = 0; i <invoice.getProducts().size() ; i++) {
            repository.insertInvoiceDetail(in_id,invoice.getProducts().get(i));
        }
        return in_id;
    }
    @Override
    public Integer deleteInvoice(Integer id) {
       return repository.deleteInvoice(id);
    }
}
