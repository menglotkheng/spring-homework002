package com.example.kheng_menglot_spring_homework002.services;

import com.example.kheng_menglot_spring_homework002.dto.InvoiceDTO;
import com.example.kheng_menglot_spring_homework002.entity.Invoice;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface InvoiceServices {


    //get by id
    Invoice getById(Integer id);

    //get all
    List<Invoice> getAll();

    //insert
    Integer insertInvoice(InvoiceDTO invoice);

    //update
    Integer updateInvoice(Integer id, @Param("invoice") InvoiceDTO invoice);

    //delete by id
    Integer deleteInvoice(Integer id);

}
