package com.example.kheng_menglot_spring_homework002.services.impl;


import com.example.kheng_menglot_spring_homework002.dto.ProductDTO;
import com.example.kheng_menglot_spring_homework002.repository.ProductRepository;
import com.example.kheng_menglot_spring_homework002.services.ProductServices;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProductServicesImpl implements ProductServices {

    private ProductRepository repository;

    //insert
    @Override
    public ProductDTO insert(ProductDTO product) {
        return repository.insert(product);
    }

    //get by id
    @Override
    public ProductDTO getById(Integer id) {
        return repository.getById(id);
    }

    //get all
    @Override
    public List<ProductDTO> getAll() {
        return repository.getAll();
    }

    //delete
    @Override
    public Integer delete(Integer id) {
        return repository.delete(id);
    }

    //update
    @Override
    public Integer update(Integer id, ProductDTO product) {
        return repository.update(id, product);
    }
}
