package com.example.kheng_menglot_spring_homework002.services.impl;

import com.example.kheng_menglot_spring_homework002.dto.CustomerDTO;
import com.example.kheng_menglot_spring_homework002.repository.CustomerRepository;
import com.example.kheng_menglot_spring_homework002.services.CustomerServices;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class CustomerServicesImpl implements CustomerServices {

    private CustomerRepository repository;


    //insert
    @Override
    public CustomerDTO insert(CustomerDTO customer) {
        return repository.insert(customer);
    }

    //get by id
    @Override
    public CustomerDTO getById(Integer id) {
        return repository.getById(id);
    }

    //get all
    @Override
    public List<CustomerDTO> getAll() {
        return repository.getAll();
    }

    //delete
    @Override
    public Integer delete(Integer id) {
        return repository.delete(id);
    }

    //update
    @Override
    public Integer update(Integer id, CustomerDTO customer) {
        return repository.update(id,customer);
    }
}
