package com.example.kheng_menglot_spring_homework002.mapper;

import com.example.kheng_menglot_spring_homework002.dto.CustomerDTO;
import com.example.kheng_menglot_spring_homework002.entity.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    CustomerDTO toDto(Customer entity);

}
